﻿create database QLKHACHSAN
go

use QLKHACHSAN

CREATE TABLE  NHANVIEN
(
 MANV VARCHAR(11) NOT NULL primary key,
 TENNV NVARCHAR(50),
 CHUCVU NVARCHAR(50),
 LUONGNV FLOAT,
 NGAYSINH DATETIME,
 GIOITINH NCHAR(3),
 CHUTHICH NVARCHAR(50),

 
 )

 CREATE TABLE DICHVU
(
 MADV VARCHAR(11) NOT NULL primary key,
 TENDV NVARCHAR(50),
 GIADV FLOAT,
 
 )

 
 CREATE TABLE PHONG
(
 MAPHONG VARCHAR(11)NOT NULL primary key,
 TENPHONG NVARCHAR(50),
 LOAIPHONG NVARCHAR(20),
 GIAPHONG FLOAT,
 CHUTHICH NVARCHAR(50),
 TINHTRANG NVARCHAR(50),
 MANV VARCHAR(11)NOT NULL,
 MADV VARCHAR(11) NOT NULL,
)

CREATE TABLE  KHACHHANG
(
 MAKH VARCHAR(11) primary key,
 TENKH NVARCHAR(50),
 CMND VARCHAR(20),
 QUOCTICH NVARCHAR(50),
 GIOITINH NCHAR (3),
 TUOI INT,
 SDT VARCHAR(20),
 MAPHONG VARCHAR(11)NOT NULL, 

 )
 
  CREATE TABLE HOADON
(
 MAHD VARCHAR(11)NOT NULL primary key,
 MANV VARCHAR(11)NOT NULL,
 MAPHONG VARCHAR(11)NOT NULL,
 MADV VARCHAR(11) NOT NULL,
 NGAY DATETIME,
 NGAY1 DATETIME,
 GIAHD FLOAT,
 )

 CREATE TABLE BAOCAO
(
 SOTT VARCHAR(11) NOT NULL primary key,
 TENKH NVARCHAR(50),
 MAPHONG VARCHAR(11),
 GIATIEN FLOAT,
 
 ) 
alter table PHONG add constraint FK_MANV foreign key(MANV) references NHANVIEN
alter table PHONG add constraint FK_MADV foreign key(MADV) references DICHVU

alter table KHACHHANG add constraint FK_MAPHONG1 foreign key(MAPHONG)references PHONG
alter table HOADON add constraint FK_MAPHONG foreign key (MAPHONG) references PHONG

-----NHANVIEN

INSERT INTO NHANVIEN VALUES ('NV1',N'Đặng Lương Tuấn Cường',N'GIÁM ĐỐC','500000','1997-01-01',N'NAM','')
INSERT INTO NHANVIEN VALUES ('NV2',N'Châu Cư Huy',N'TRƯỞNG CA','40000000','1997-08-27',N'NAM','')
INSERT INTO NHANVIEN VALUES ('NV3',N'Nguyễn Ngọc Hoàng Ân',N'NHÂN VIÊN DỌN PHÒNG','120000','1999-07-23',N'NỮ','')
INSERT INTO NHANVIEN VALUES ('NV4',N'Trương Quốc Hà',N'NHÂN VIÊN DỌN PHÒNG','120000','2001-08-08',N'NAM','')
INSERT INTO NHANVIEN VALUES ('NV5',N'Nguyễn Thanh Hải',N'NHÂN VIÊN DỌN PHÒNG','120000','1999-04-08',N'NAM','')
INSERT INTO NHANVIEN VALUES ('NV6',N'Thiên Trần Tuấn',N'BẢO VỆ','10000000','2002-11-12',N'NAM','')
INSERT INTO NHANVIEN VALUES ('NV7',N'Nguyễn Ngọc Hoàng An',N'BẢO VỆ','100000','1999-04-08',N'NỮ','')
INSERT INTO NHANVIEN VALUES ('NV8',N'Bách Thanh Tùng',N'BẾP TRƯỞNG','3000000','1987-07-29',N'NAM','')
INSERT INTO NHANVIEN VALUES ('NV9',N'Văn Bảo Toàn',N'PHÓ BẾP','2000000','1989-06-28',N'NAM','')
INSERT INTO NHANVIEN VALUES ('NV10',N'Trần Gia An',N'PHỤ BẾP','1200000','1999-06-25',N'NỮ','')
INSERT INTO NHANVIEN VALUES ('NV11',N'Trần Văn Lâm',N'PHỤ BẾP','1200000','2003-03-01',N'NAM','')
INSERT INTO NHANVIEN VALUES ('NV12',N'Nguyễn Hồng Quân',N'LỄ TÂN','1500000','2000-03-02',N'NAM','')
INSERT INTO NHANVIEN VALUES ('NV13',N'Sẫm Thục My',N'LỄ TÂN','15000000','1999-05-30',N'NỮ','')
INSERT INTO NHANVIEN VALUES ('NV14',N'Thiệu Thiên An',N'LỄ TÂN','1500000','1999-04-08',N'NỮ','')
INSERT INTO NHANVIEN VALUES ('NV15',N'Lâm Thái Hòa',N'LỄ TÂN','1000000','2000-09-29',N'NAM','')
INSERT INTO NHANVIEN VALUES ('NV16',N'Nguyễn Minh Thảo',N'NHÂN VIÊN VỆ SINH','110000','1999-04-08',N'NỮ','')
INSERT INTO NHANVIEN VALUES ('NV17',N'Hoàng Hồng Nguyên',N'NHÂN VIÊN VỆ SINH','1100000','2004-09-18',N'NỮ','')
INSERT INTO NHANVIEN VALUES ('NV18',N'Thái Hiếu Đức ',N'NHÂN VIÊN VỆ SINH','1100000','1988-12-12',N'NAM','')
INSERT INTO NHANVIEN VALUES ('NV19',N'Trương Quốc Vinh',N'QUẢN LÝ','2000000','1998-04-18',N'NAM','')
INSERT INTO NHANVIEN VALUES ('NV20',N'La Mỹ Linh',N'QUẢN LÝ','2000000','2000-10-20',N'NỮ','')
select * from NHANVIEN

------DICHVU
INSERT INTO DICHVU VALUES ('DV1',N'MASSAGE','1000000')
INSERT INTO DICHVU VALUES ('DV2',N'CHĂM SÓC DA','300000')
INSERT INTO DICHVU VALUES ('DV3',N'TẮM BÙN','300000')
INSERT INTO DICHVU VALUES ('DV4',N'PHỤC VỤ ĂN UỐNG','600000')
INSERT INTO DICHVU VALUES ('DV5',N'TẮM HƠI','650000')
INSERT INTO DICHVU VALUES ('DV6',N'TRỊ MỤN','450000')

INSERT INTO DICHVU VALUES ('DV7',N'BỂ BƠI 4 MÙA','600000')
INSERT INTO DICHVU VALUES ('DV8',N'GIẶT ỦI QUẦN ÁO','500000')

INSERT INTO DICHVU VALUES ('DV9',N'FITNESS CENTRE','750000')
INSERT INTO DICHVU VALUES ('DV10',N'SÂN GOLF','850000')

INSERT INTO DICHVU VALUES ('DV11',N'XE ĐƯA ĐÓN SÂN BAY','1500000')
INSERT INTO DICHVU VALUES ('DV12',N'QUẦY BAR','2000000')
INSERT INTO DICHVU VALUES ('DV13',N'KARAOKE','1000000')
select * from DICHVU

--PHONG
INSERT INTO PHONG VALUES (N'MP01','T1',N'THỪƠNG','2500000',N'KHÔNG CÓ MÁY LẠNH',N'THUÊ PHÒNG','NV12','DV1')
INSERT INTO PHONG VALUES (N'MP02','T1',N'THỪƠNG','2500000',N'KHÔNG CÓ MÁY LẠNH',N'THUÊ PHÒNG','NV12','DV1')

INSERT INTO PHONG VALUES (N'MP03','T1',N'THỪƠNG','2500000',N'KHÔNG CÓ MÁY LẠNH',N'THUÊ PHÒNG','NV13','DV2')
INSERT INTO PHONG VALUES (N'MP04','T1',N'THỪƠNG','2500000',N'KHÔNG CÓ MÁY LẠNH',N'THUÊ PHÒNG','NV13','DV2')
INSERT INTO PHONG VALUES (N'MP05','T1',N'THỪƠNG','2500000',N'KHÔNG CÓ MÁY LẠNH',N'THUÊ PHÒNG','NV13','DV2')

INSERT INTO PHONG VALUES (N'MP06','T1',N'THỪƠNG','2500000',N'KHÔNG CÓ MÁY LẠNH',N'THUÊ PHÒNG','NV12','DV1')
INSERT INTO PHONG VALUES (N'MP07','T1',N'THỪƠNG','2500000',N'KHÔNG CÓ MÁY LẠNH',N'THUÊ PHÒNG','NV12','DV3')
INSERT INTO PHONG VALUES (N'MP08','T1',N'THỪƠNG','2500000',N'KHÔNG CÓ MÁY LẠNH',N'THUÊ PHÒNG','NV12','DV3')

INSERT INTO PHONG VALUES (N'MP09','T1',N'THỪƠNG','2500000',N'KHÔNG CÓ MÁY LẠNH',N'THUÊ PHÒNG','NV13','DV4')
INSERT INTO PHONG VALUES (N'MP10','T1',N'THỪƠNG','2500000',N'KHÔNG CÓ MÁY LẠNH',N'THUÊ PHÒNG','NV13','DV4')
INSERT INTO PHONG VALUES (N'MP11','T1',N'THỪƠNG','2500000',N'KHÔNG CÓ MÁY LẠNH',N'THUÊ PHÒNG','NV13','DV4')

INSERT INTO PHONG VALUES (N'MP12','T1',N'THỪƠNG','2500000',N'KHÔNG CÓ MÁY LẠNH',N'THUÊ PHÒNG','NV14','DV5')
INSERT INTO PHONG VALUES (N'MP13','T1',N'THỪƠNG','2500000',N'KHÔNG CÓ MÁY LẠNH',N'THUÊ PHÒNG','NV14','DV5')

INSERT INTO PHONG VALUES (N'MP14','T2',N'VIP','5000000',N'CÓ MÁY LẠNH',N'THUÊ PHÒNG','NV14','DV7')
INSERT INTO PHONG VALUES (N'MP15','T2',N'VIP','5000000',N'CÓ MÁY LẠNH',N'THUÊ PHÒNG','NV14','DV8')

INSERT INTO PHONG VALUES (N'MP16',N'T2',N'VIP','5000000',N'CÓ MÁY LẠNH',N'THUÊ PHÒNG','NV14','DV9')
INSERT INTO PHONG VALUES (N'MP17',N'T2',N'VIP','5000000',N'CÓ MÁY LẠNH',N'THUÊ PHÒNG','NV15','DV10')

INSERT INTO PHONG VALUES (N'MP18',N'T2',N'VIP','5000000',N'CÓ MÁY LẠNH',N'THUÊ PHÒNG','NV15','DV11')
INSERT INTO PHONG VALUES (N'MP19',N'T2',N'VIP','5000000',N'CÓ MÁY LẠNH',N'THUÊ PHÒNG','NV15','DV12')
INSERT INTO PHONG VALUES (N'MP20',N'T2',N'VIP','5000000',N'CÓ MÁY LẠNH',N'THUÊ PHÒNG','NV15','DV13')



----KHACHHANG
INSERT INTO KHACHHANG VALUES (N'KH01',N'Nguyễn Huy Khương','098763423224',N'Việt Nam',N'Nam','29','0809456789','MP01')
INSERT INTO KHACHHANG VALUES (N'KH02',N'Trần Ngọc Hân','085634388439',N'Việt Nam',N'Nữ','32','0938444567','MP02')
INSERT INTO KHACHHANG VALUES (N'KH03',N'Hà Duy Lập','085558904567',N'Việt Nam',N'Nam','25','0938123763','MP03')
INSERT INTO KHACHHANG VALUES (N'KH04',N'Phan Thị Thanh','081235674512',N'Việt Nam',N'Nữ','21','0895432213','MP04')
INSERT INTO KHACHHANG VALUES (N'KH05',N'Lê Nhật Minh','097764423334',N'Việt Nam',N'Nam','35','0822978967','MP05')
INSERT INTO KHACHHANG VALUES (N'KH06',N'Lê Hoài Thương','089789423335',N'Việt Nam',N'Nữ','21','0812356710','MP06')
INSERT INTO KHACHHANG VALUES (N'KH07',N'Trần Minh Long','098756845824',N'Việt Nam',N'Nam','23','0809654279','MP07')
INSERT INTO KHACHHANG VALUES (N'KH08',N'Trần Ngọc Linh','097561233342',N'Việt Nam',N'Nữ','35','0805426123','MP08')
INSERT INTO KHACHHANG VALUES (N'KH09',N'Ngô Thanh Tuấn','098864588834',N'Việt Nam',N'Nam','28','0987783983','MP09')
INSERT INTO KHACHHANG VALUES (N'KH10',N'Ngô Văn Tâm','098995478444',N'Việt Nam',N'Nam','40','0709888123','MP10')

INSERT INTO KHACHHANG VALUES (N'KH11',N'Nguyễn Thu Hà','097733422554',N'Việt Nam',N'Nữ','45','0809447944','MP11')
INSERT INTO KHACHHANG VALUES (N'KH12',N'Trần Anh Tuấn','094762426664',N'Việt Nam',N'Nam','50','0809789666','MP12')
INSERT INTO KHACHHANG VALUES (N'KH13',N'Nguyễn Thị Thu Thủy','098755429454',N'Việt Nam',N'Nữ','20','0895954324','MP13')
INSERT INTO KHACHHANG VALUES (N'KH14',N'Thiệu Thiên Đức','202119621312',N'Việt Nam',N'Nam','60','0938121379','MP14')
INSERT INTO KHACHHANG VALUES (N'KH15',N'Trần Thanh Triều','097763322114',N'Việt Nam',N'Nữ','23','0806455589','MP15')
SELECT * FROM khachhang where MAKH='KH01'
INSERT INTO KHACHHANG VALUES (N'KH16',N'Quang Vĩnh Khang','068763434533',N'Trung Quốc',N'Nam','18','060556688','MP16')
INSERT INTO KHACHHANG VALUES (N'KH17',N'Lưu Chí Kiệt','068763433435',N'Trung Quốc',N'Nam','24','0604558549','MP17')

INSERT INTO KHACHHANG VALUES (N'KH18',N'Kim Jisoo','019819950103',N'Hàn Quốc',N'Nữ','27','0301162016','MP18')
INSERT INTO KHACHHANG VALUES (N'KH19',N'Ariana Grande','048726061993',N'Hoa Kỳ',N'Nữ','29','0609199306','MP19')
INSERT INTO KHACHHANG VALUES (N'KH20',N'Harry Styles','028701021994',N'Vương Quốc Anh',N'Nam','28','0850211994','MP20')
select * from KHACHHANG
select * from HOADON
-----HOADON
INSERT INTO HOADON VALUES (N'HD01',N'NV12',N'MP01','DV1',N'2022/01/02','2022/01/07','2500000')
INSERT INTO HOADON VALUES (N'HD02',N'NV12',N'MP02','DV1',N'2022/02/09','2022/02/13','2500000')

INSERT INTO HOADON VALUES (N'HD03',N'NV13',N'MP03','DV2',N'2022/04/02','2022/04/08','2500000')
INSERT INTO HOADON VALUES (N'HD04',N'NV13',N'MP04','DV2',N'2022/04/17','2022/04/21','2500000')
INSERT INTO HOADON VALUES (N'HD05',N'NV13',N'MP05','DV2',N'2022/04/22','2022/04/30','2500000')

INSERT INTO HOADON VALUES (N'HD06',N'NV12',N'MP06','DV1',N'2022/03/14','2022/03/18','2500000')
INSERT INTO HOADON VALUES (N'HD07',N'NV12',N'MP07','DV3',N'2022/05/22','2022/05/30','2500000')
INSERT INTO HOADON VALUES (N'HD08',N'NV12',N'MP08','DV3',N'2022/06/15','2022/06/22','2500000')

INSERT INTO HOADON VALUES (N'HD09',N'NV13',N'MP09','DV4',N'2022/03/15','2022/03/20','2500000')
INSERT INTO HOADON VALUES (N'HD10',N'NV13',N'MP10','DV4',N'2022/01/18','2022/01/28','2500000')
INSERT INTO HOADON VALUES (N'HD11',N'NV13',N'MP11','DV4',N'2022/06/18','2022/06/24','2500000')

INSERT INTO HOADON VALUES (N'HD12',N'NV14',N'MP12','DV5',N'2022/05/27','2022/06/04','2500000')
INSERT INTO HOADON VALUES (N'HD13',N'NV14',N'MP13','DV5',N'2022/02/22','2022/03/08','2500000')
INSERT INTO HOADON VALUES (N'HD14',N'NV14',N'MP14','DV7',N'2022/01/14','2022/01/24','5000000')
INSERT INTO HOADON VALUES (N'HD15',N'NV14',N'MP15','DV8',N'2022/04/16','2022/05/28','5000000')

INSERT INTO HOADON VALUES (N'HD16',N'NV14',N'MP16','DV9',N'2022/02/22','2022/03/28','5000000')
INSERT INTO HOADON VALUES (N'HD17',N'NV15',N'MP17','DV10',N'2022/04/23','2022/05/10','5000000')

INSERT INTO HOADON VALUES (N'HD18',N'NV15',N'MP18','DV11',N'2022/01/03','2022/01/19','5000000')
INSERT INTO HOADON VALUES (N'HD19',N'NV15',N'MP19','DV12',N'2022/06/06','2022/06/29','5000000')
INSERT INTO HOADON VALUES (N'HD20',N'NV15',N'MP20','DV13','2022/03/22','2022/03/30','5000000')


------------------
INSERT INTO BAOCAO VALUES ('1',N'Nguyễn Thu Hà','MP11','5000000')
INSERT INTO BAOCAO VALUES ('2',N'Trần Anh Tuấn','MP12','5000000')
INSERT INTO BAOCAO VALUES ('3',N'Nguyễn Thị Thu Thủy','MP13','5000000')
INSERT INTO BAOCAO VALUES ('4',N'Thiệu Thiên Đức','MP14','5000000')
INSERT INTO BAOCAO VALUES ('5',N'Trần Thanh Triều','MP15','5000000')
INSERT INTO BAOCAO VALUES ('6',N'Quang Vĩnh Khang','MP16','5000000')
INSERT INTO BAOCAO VALUES ('7',N'Lưu Chí Kiệt','MP17','5000000')
INSERT INTO BAOCAO VALUES ('8',N'Harry Styles','MP20','5000000')
INSERT INTO BAOCAO VALUES ('9',N'Ariana Grande','MP19','5000000')
INSERT INTO BAOCAO VALUES ('10',N'Kim Jisoo','MP18','5000000')



select * from BAOCAO
select * from PHONG
select * from KHACHHANG
SELECT * FROM DICHVU where MADV='DV1'