/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package businessLayer;

import static businessLayer.MyConnection.getKetNoi;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;

/**
 *
 * @author khoa
 */
public class MyConnection {
   private static Connection ketNoi;
	public static Connection getKetNoi()
	{
	         try
	         {
                    Class.forName("com.microsoft.sqlserver.jdbc.SQLServerDriver");
                    String url="jdbc:sqlserver://DESKTOP-QE0DIKT\\SQLEXPRESS:1433;databaseName=QLKHACHSAN;encrypt=true;trustServerCertificate=true;";
                    ketNoi=DriverManager.getConnection(url,"sa","sa");
	          }
                          catch(ClassNotFoundException | SQLException e)
   	         {
                    e.printStackTrace();
	         }
	         return ketNoi;
	}

    public static void closeConnection(java.sql.Connection c) {
        try {
            c.close();
        } catch (Exception e) {
            e.printStackTrace();
            // TODO: handle exception
        }
    }
     public static boolean UpdateKH(dataTransferObject.KhachHang kh){
       String sql="Update KHACHHANG set TENKH=?,CMND=?,QUOCTICH=?,GIOITINH=?,TUOI=?,SDT=?,MAPHONG=? where MAKH=?";
       try
       {
           ketNoi = getKetNoi();
           PreparedStatement ps=ketNoi.prepareStatement(sql);
            ps.setString(8,kh.getMAKH());
            ps.setString(1,kh.getTENKH());
            ps.setString(2,kh.getCMND());
            ps.setString(3,kh.getQUOCTICH());
            ps.setString(4,kh.getGIOITINH());
            ps.setInt(5,kh.getTuoi());
            ps.setString(6,kh.getSDT());
            ps.setString(7,kh.getMAPHONG());
            return ps.executeUpdate()>0;
   }
        catch(SQLException e){}
        return false;    
   }
     public static boolean UpdateNV(dataTransferObject.NhanVien nv){
       String sql="Update NhanVien set TENNV=?,CHUCVU=?,LUONGNV=?,NGAYSINH=?,GIOITINH=?,CHUTHICH=? where MANV=?";
       try
       {
           ketNoi = getKetNoi();
           PreparedStatement ps=ketNoi.prepareStatement(sql);
            ps.setString(7,nv.getMANV());
            ps.setString(1,nv.getTENNV());
            ps.setString(2,nv.getCHUCVU());
            ps.setFloat(3,nv.getLUONG());
            ps.setString(4,nv.getNGAYSINH());
            ps.setString(5,nv.getGIOITINH());
            ps.setString(6,nv.getCHUTHICH());
            return ps.executeUpdate()>0;
   }
        catch(SQLException e){}
        return false;    
   }

    public static boolean UpdatePN(dataTransferObject.Phong p) {
       String sql="Update Phong set TENPHONG=?,LOAIPHONG=?,GIAPHONG=?,CHUTHICH=?,TINHTRANG=?,MANV=?,MADV=? where MAPHONG=?";
       try
       {
           ketNoi = getKetNoi();
           PreparedStatement ps=ketNoi.prepareStatement(sql);
            ps.setString(8,p.getMAPHONG());
            ps.setString(1,p.getTENPHONG());
            ps.setString(2,p.getLOAIPHONG());
            ps.setFloat(3,p.getGIAPHONG());
            ps.setString(4,p.getCHUTHICH());
            ps.setString(5,p.getTINHTRANG());
            ps.setString(6,p.getMANV());
            ps.setString(7,p.getMADV());
            return ps.executeUpdate()>0;
   }
        catch(SQLException e){}
        return false;    
   }
    
    public static boolean UpdateHD(dataTransferObject.HoaDon hd) {
       String sql="Update HoaDon set MANV=?,MAPHONG=?,MADV=?,NGAY=?,NGAY1=?,GIAHD=? where MAHD=?";
       try
       {
           ketNoi = getKetNoi();
           PreparedStatement ps=ketNoi.prepareStatement(sql);
            ps.setString(7,hd.getMAHD());
            ps.setString(1,hd.getMANV());
            ps.setString(2,hd.getMAPHONG());
            ps.setString(3,hd.getMADV());
            ps.setString(4, hd.getNGAY());
            ps.setString(5,hd.getNGAY1());
            ps.setFloat(6,hd.getGIAHD());
           
            return ps.executeUpdate()>0;
   }
        catch(SQLException e){}
        return false;    
   }

    public static boolean UpdateDV(dataTransferObject.DichVu dv) {
       String sql="Update DichVu set TENDV=?,GIADV=? where MADV=?";
       try
       {
           ketNoi = getKetNoi();
           PreparedStatement ps=ketNoi.prepareStatement(sql);
            ps.setString(3,dv.getMADV());
            ps.setString(1,dv.getTENNVL());
            ps.setFloat(2,dv.getGIADV());
            
           
            return ps.executeUpdate()>0;
   }
        catch(SQLException e){}
        return false;    
   }

public static boolean UpdateBC(dataTransferObject.BaoCao bc) {
       String sql="Update BaoCao set TENKH=?,MAPHONG=?,GIATIEN=? where SOTT=?";
       try
       {
           ketNoi = getKetNoi();
           PreparedStatement ps=ketNoi.prepareStatement(sql);
            ps.setString(4,bc.getSOTT());
            ps.setString(1,bc.getTENKH());
            ps.setString(2,bc.getMAPHONG());
            ps.setFloat(3,bc.getGIATIEN());
            
           
            return ps.executeUpdate()>0;
   }
        catch(SQLException e){}
        return false;    
   }

 
}