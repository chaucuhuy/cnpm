/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataTransferObject;

import java.util.Date;

/**
 *
 * @author khoa
 */
public class NhanVien {
  
    private String MANV;
    
    private String TENNV;
    
    private String CHUCVU;
    
    private String GIOITINH;
    
    private float LUONG;
    
    private String NGAYSINH;
    
    private String CHUTHICH;
    /**
     * Get the value of CHUTHICH
     *
     * @return the value of CHUTHICH
     */
    public String getCHUTHICH() {
        return CHUTHICH;
    }

    /**
     * Set the value of CHUTHICH
     *
     * @param CHUTHICH new value of CHUTHICH
     */
    public void setCHUTHICH(String CHUTHICH) {
        this.CHUTHICH = CHUTHICH;
    }


    /**
     * Get the value of NGAYSINH
     *
     * @return the value of NGAYSINH
     */
    public String getNGAYSINH() {
        return NGAYSINH;
    }

    /**
     * Set the value of NGAYSINH
     *
     * @param NGAYSINH new value of NGAYSINH
     */
    public void setNGAYSINH(String NGAYSINH) {
        this.NGAYSINH = NGAYSINH;
    }


    /**
     * Get the value of LUONG
     *
     * @return the value of LUONG
     */
    public float getLUONG() {
        return LUONG;
    }

    /**
     * Set the value of LUONG
     *
     * @param LUONG new value of LUONG
     */
    public void setLUONG(float LUONG) {
        this.LUONG = LUONG;
    }


    /**
     * Get the value of GIOITINH
     *
     * @return the value of GIOITINH
     */
    public String getGIOITINH() {
        return GIOITINH;
    }

    /**
     * Set the value of GIOITINH
     *
     * @param GIOITINH new value of GIOITINH
     */
    public void setGIOITINH(String GIOITINH) {
        this.GIOITINH = GIOITINH;
    }


    /**
     * Get the value of CHUCVU
     *
     * @return the value of CHUCVU
     */
    public String getCHUCVU() {
        return CHUCVU;
    }

    /**
     * Set the value of CHUCVU
     *
     * @param CHUCVU new value of CHUCVU
     */
    public void setCHUCVU(String CHUCVU) {
        this.CHUCVU = CHUCVU;
    }


    /**
     * Get the value of TENNV
     *
     * @return the value of TENNV
     */
    public String getTENNV() {
        return TENNV;
    }

    /**
     * Set the value of TENNV
     *
     * @param TENNV new value of TENNV
     */
    public void setTENNV(String TENNV) {
        this.TENNV = TENNV;
    }


    /**
     * Get the value of MANV
     *
     * @return the value of MANV
     */
    public String getMANV() {
        return MANV;
    }

    /**
     * Set the value of MANV
     *
     * @param MANV new value of MANV
     */
    public void setMANV(String MANV) {
        this.MANV = MANV;
    }

    public NhanVien(String MANV, String TENNV, String CHUCVU, String GIOITINH, float LUONG, String NGAYSINH, String CHUTHICH) {
        this.MANV = MANV;
        this.TENNV = TENNV;
        this.CHUCVU = CHUCVU;
        this.GIOITINH = GIOITINH;
        this.LUONG = LUONG;
        this.NGAYSINH = NGAYSINH;
        this.CHUTHICH = CHUTHICH;
    }
    
    

    public NhanVien() {
    }
 
    
}