/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataTransferObject;

/**
 *
 * @author khoa
 */
public class DichVu {
   
    private String MADV;
    
    private String TENNVL;
    
    private float GIADV;

    

    public String getMADV() {
        return MADV;
    }

    public void setMADV(String MADV) {
        this.MADV = MADV;
    }

    public String getTENNVL() {
        return TENNVL;
    }

    public void setTENNVL(String TENNVL) {
        this.TENNVL = TENNVL;
    }

    public float getGIADV() {
        return GIADV;
    }

    public void setGIADV(float GIADV) {
        this.GIADV = GIADV;
    }

  
    public DichVu(String MADV, String TENNVL, float GIADV) {
        this.MADV = MADV;
        this.TENNVL = TENNVL;
        this.GIADV = GIADV;
    }

    public DichVu() {
    }   

    @Override
    public String toString() {
        return "DichVu{" + "MADV=" + MADV + ", TENNVL=" + TENNVL + ", GIADV=" + GIADV + '}';
    }
    
    
    
}
