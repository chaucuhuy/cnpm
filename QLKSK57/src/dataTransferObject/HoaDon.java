/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataTransferObject;

import java.util.Date;

/**
 *
 * @author khoa
 */
public class HoaDon {
    private String MAHD;
    private String MANV;
    private String MAPHONG;
    private String MADV;
    private String NGAY;
    private String NGAY1;
    private float GIAHD;

    public String getMAHD() {
        return MAHD;
    }

    public void setMAHD(String MAHD) {
        this.MAHD = MAHD;
    }

    public String getMANV() {
        return MANV;
    }

    public void setMANV(String MANV) {
        this.MANV = MANV;
    }

    public String getMAPHONG() {
        return MAPHONG;
    }

    public void setMAPHONG(String MAPHONG) {
        this.MAPHONG = MAPHONG;
    }

    public String getMADV() {
        return MADV;
    }

    public void setMADV(String MADV) {
        this.MADV = MADV;
    }

    public String getNGAY() {
        return NGAY;
    }

    public void setNGAY(String NGAY) {
        this.NGAY = NGAY;
    }

    public String getNGAY1() {
        return NGAY1;
    }

    public void setNGAY1(String NGAY1) {
        this.NGAY1 = NGAY1;
    }

    public float getGIAHD() {
        return GIAHD;
    }

    public void setGIAHD(float GIAHD) {
        this.GIAHD = GIAHD;
    }

    
public HoaDon(String MAHD, String MANV, String MAPHONG, String MADV, String NGAY, String NGAY1, float GIAHD) {
        this.MAHD = MAHD;
        this.MANV = MANV;
        this.MAPHONG = MAPHONG;
        this.MADV = MADV;
        this.NGAY = NGAY;
        this.NGAY1 = NGAY1;
        this.GIAHD = GIAHD;
    }
    
    

    public HoaDon() {
    }
 
    
}
    
    