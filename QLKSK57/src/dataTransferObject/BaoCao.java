/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataTransferObject;

/**
 *
 * @author tieua
 */
public class BaoCao {
    
    private String SOTT;
    private String TENKH;
    private String MAPHONG;
    private float GIATIEN;

    
   
    public String getSOTT() {
        return SOTT;
    }

    public void setSOTT(String SOTT) {
        this.SOTT = SOTT;
    }

    public String getTENKH() {
        return TENKH;
    }

    public void setTENKH(String TENKH) {
        this.TENKH = TENKH;
    }

    public String getMAPHONG() {
        return MAPHONG;
    }

    public void setMAPHONG(String MAPHONG) {
        this.MAPHONG = MAPHONG;
    }

    public float getGIATIEN() {
        return GIATIEN;
    }

    public void setGIATIEN(float GIATIEN) {
        this.GIATIEN = GIATIEN;
    }
    public BaoCao(String SOTT, String TENKH,String MAPHONG, float GIATIEN) {
        this.SOTT = SOTT;
        this.TENKH = TENKH;
        this.MAPHONG = MAPHONG;
        this.GIATIEN = GIATIEN;
    }
     public BaoCao() {
    } 
}

    

    
    

